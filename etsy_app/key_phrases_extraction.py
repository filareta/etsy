import itertools, nltk, string, gensim, re, regex
from nltk.stem.wordnet import WordNetLemmatizer
from nltk.corpus import wordnet
from etsy_app.document import Document
from nltk.chunk.regexp import RegexpChunkRule, RegexpChunkParser


#Taken from Su Nam Kim Paper...
grammar = r"""
    NBAR:
        {<NN.*|JJ>*<NN.*>}  # Nouns and Adjectives, terminated with Nouns
        
    NP:
        {<NBAR>}
        {<NBAR><IN><NBAR>}  # Above, connected with in/of/etc...
"""

grammar2 = r'NP: {<NN.*|JJ>*<NN.*> (<IN> <NN.*|JJ>*<NN.*>)?}'

def get_wordnet_pos(treebank_tag):
    if treebank_tag.startswith('J'):
        return wordnet.ADJ
    elif treebank_tag.startswith('V'):
        return wordnet.VERB
    elif treebank_tag.startswith('N'):
        return wordnet.NOUN
    elif treebank_tag.startswith('R'):
        return wordnet.ADV
    else:
        return ''

def extract_candidate_chunks(text, stop_words=None, grammar=r'{<JJ>*<NN>+}'):
    # exclude candidates that are stop words or entirely punctuation
    lmtz = WordNetLemmatizer()

    text = re.sub(r'([^:]+:)?//([^.]+\.)+[^/]+/([^/\s]+/)*([^/\s]+)', ' ', text)
    text = re.sub(r'([*!@#$%=_/~\-+:|\[\]\'\(\)_]+|\.{2,})', ' ', text)
    text = re.sub(u"[\u0400-\u0500]+", ' ', text)

    punct = set(string.punctuation)
    stop_words = stop_words or set(nltk.corpus.stopwords.words('english'))
    # tokenize, POS-tag, and chunk using regular expressions
    chunk_rule = RegexpChunkRule.fromstring(grammar)
    chunker = nltk.chunk.regexp.RegexpChunkParser([chunk_rule], chunk_label='NP')
    sents = [nltk.word_tokenize(sent) for sent in nltk.sent_tokenize(text)]

    sents = [[x for x in sent if x not in set(['ll', 's', 'd', 't'])] for sent in sents]
    tagged_sents = nltk.pos_tag_sents(sents)
    
    all_chunks = list(itertools.chain.from_iterable(nltk.chunk.tree2conlltags(chunker.parse(tagged_sent))
                                                    for tagged_sent in tagged_sents))


    keyfunc = lambda triple: triple[2] != 'O'
    grouped  = itertools.groupby(all_chunks, keyfunc)
    candidates = []

    single_word = lambda token: len(re.split(r'\s+', token)) == 1
          
    # join constituent chunk words into a single chunked phrase
    for key, group in grouped:
        if key:
            group_items = [(word, pos) for word, pos, chunk in group]

            if all([word.lower() in stop_words for word, pos in group_items]):
                continue

            token = ' '.join([word for word, pos in group_items]).lower()
        
            if single_word(token):
                pos = group_items[0][1]
                token = lemmatize_word(token, pos, lmtz)
            
            if token is not None:
                candidates.append(regex.sub(u"\p{P}+", "", token))


    filtered_cand = [cand for cand in candidates
                    if cand not in stop_words and not all(char in punct for char in cand)
                        and not any(map(lambda x: x in cand, ['http', 'www', '.com', 'pdf', 'jpg', 'url']))
                        and len(cand) > 2]

    return filtered_cand


def lemmatize_word(token_cand, tag, lmtz):
    wordnet_tag = get_wordnet_pos(tag)

    if wordnet_tag != '':
        return lmtz.lemmatize(token_cand, wordnet_tag)


def extract_candidate_words(sentence, stop_words=None, good_tags=set(['JJ','JJR','JJS','NN','NNP','NNS','NNPS'])):
    # exclude candidates that are stop words or entirely punctuation
    lmtz = WordNetLemmatizer()

    sentence = re.sub(r'([^:]+:)?//([^.]+\.)+[^/]+/([^/\s]+/)*([^/\s]+)', ' ', sentence)
    sentence = re.sub(r'([*!@#$%=_/~\-+:|\[\]\'\(\)_]+|\.{2,})', ' ', sentence)
    sentence = re.sub(u"[\u0400-\u0500]+", ' ', sentence)

    punct = set(string.punctuation)

    if not stop_words:
        stop_words = set(nltk.corpus.stopwords.words('english'))
    # tokenize and POS-tag words
    tagged_words = nltk.pos_tag(nltk.word_tokenize(sentence))

    # filter on certain POS tags and lowercase all words
    candidates = [lemmatize_word(word.lower(), tag, lmtz) for word, tag in tagged_words 
                  if tag in good_tags and word.lower() not in stop_words 
                  and not all(char in punct for char in word) and len(word) > 2]

    return candidates


def score_keyphrases_by_tfidf(texts, stop_words=None, grammar=None, candidates='chunks'):
    # extract candidates from each text in texts, either chunks or words
    if candidates == 'chunks':
        if grammar is not None:
            boc_texts = [extract_candidate_chunks(text, stop_words, grammar) for text in texts]
        else:
            boc_texts = [extract_candidate_chunks(text, stop_words=stop_words) for text in texts]
    
    # make gensim dictionary and corpus
    dictionary = gensim.corpora.Dictionary(boc_texts)
    corpus = [dictionary.doc2bow(boc_text) for boc_text in boc_texts]
    # transform corpus with tf*idf model
    tfidf = gensim.models.TfidfModel(corpus, id2word=dictionary, normalize=True)
    corpus_tfidf = tfidf[corpus]
    
    return corpus_tfidf, dictionary


def store_tfidf_model(texts, corpus_path, stop_words=None, grammar=None, candidates='chunks'):
    # extract candidates from each text in texts, either chunks or words
    if candidates == 'chunks':
        if grammar is not None:
            boc_texts = [extract_candidate_chunks(text, stop_words, grammar) for text in texts]
        else:
            boc_texts = [extract_candidate_chunks(text, stop_words=stop_words) for text in texts]
    
    # make gensim dictionary and corpus

    dictionary = gensim.corpora.Dictionary(boc_text)
    corpus = [dictionary.doc2bow(boc_text) for boc_text in boc_texts]
    # transform corpus with tf*idf model
    tfidf = gensim.models.TfidfModel(corpus, id2word=dictionary, normalize=True)

    tfidf.save(corpus_path)


def score_words_in_text(text, corpus_path, stop_words, topn):
    boc_text = extract_candidate_chunks(text, stop_words=stop_words)

    tfidf = gensim.models.TfidfModel.load(corpus_path)
    dictionary = tfidf.id2word

    text_bow = dictionary.doc2bow(boc_text)
    sorted_vec_best = sorted(tfidf[text_bow], key=lambda tup: tup[1], reverse=True)[:topn]

    return [(dictionary.get(key_id), metric) for (key_id, metric) in sorted_vec_best]


# if __name__ == '__main__':
    #TODO: LOAD from DB per categories



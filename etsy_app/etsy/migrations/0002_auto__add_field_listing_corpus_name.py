# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Listing.corpus_name'
        db.add_column('etsy_listing', 'corpus_name',
                      self.gf('django.db.models.fields.CharField')(max_length=100, default=''),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'Listing.corpus_name'
        db.delete_column('etsy_listing', 'corpus_name')


    models = {
        'etsy.category': {
            'Meta': {'object_name': 'Category'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        },
        'etsy.listing': {
            'Meta': {'ordering': "['-views']", 'object_name': 'Listing'},
            'category': ('django.db.models.fields.related.ForeignKey', [], {'null': 'True', 'to': "orm['etsy.Category']"}),
            'corpus_name': ('django.db.models.fields.CharField', [], {'max_length': '100', 'default': "''"}),
            'description': ('django.db.models.fields.CharField', [], {'max_length': '2000'}),
            'document_index': ('django.db.models.fields.IntegerField', [], {'default': '-1'}),
            'favorers': ('django.db.models.fields.BigIntegerField', [], {'null': 'True'}),
            'has_variations': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'height': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'decimal_places': '3', 'max_digits': '10'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_customizable': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'length': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'decimal_places': '3', 'max_digits': '10'}),
            'listing_id': ('django.db.models.fields.BigIntegerField', [], {'unique': 'True'}),
            'made_by': ('django.db.models.fields.CharField', [], {'null': 'True', 'max_length': '20'}),
            'materials': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': "orm['etsy.Material']"}),
            'occasion': ('django.db.models.fields.CharField', [], {'null': 'True', 'max_length': '20'}),
            'price': ('django.db.models.fields.DecimalField', [], {'decimal_places': '3', 'max_digits': '10'}),
            'quantity': ('django.db.models.fields.IntegerField', [], {'null': 'True'}),
            'recipient': ('django.db.models.fields.CharField', [], {'null': 'True', 'max_length': '20'}),
            'styles': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': "orm['etsy.Style']"}),
            'tags': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': "orm['etsy.Tag']"}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'views': ('django.db.models.fields.BigIntegerField', [], {'null': 'True'}),
            'weight': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'decimal_places': '3', 'max_digits': '10'}),
            'width': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'decimal_places': '3', 'max_digits': '10'})
        },
        'etsy.material': {
            'Meta': {'object_name': 'Material'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        },
        'etsy.style': {
            'Meta': {'object_name': 'Style'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        },
        'etsy.tag': {
            'Meta': {'object_name': 'Tag'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        }
    }

    complete_apps = ['etsy']
# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Tag'
        db.create_table('etsy_tag', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=200)),
        ))
        db.send_create_signal('etsy', ['Tag'])

        # Adding model 'Category'
        db.create_table('etsy_category', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=200)),
        ))
        db.send_create_signal('etsy', ['Category'])

        # Adding model 'Style'
        db.create_table('etsy_style', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=200)),
        ))
        db.send_create_signal('etsy', ['Style'])

        # Adding model 'Material'
        db.create_table('etsy_material', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=200)),
        ))
        db.send_create_signal('etsy', ['Material'])

        # Adding model 'Listing'
        db.create_table('etsy_listing', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('description', self.gf('django.db.models.fields.CharField')(max_length=2000)),
            ('width', self.gf('django.db.models.fields.DecimalField')(max_digits=10, decimal_places=3, null=True)),
            ('height', self.gf('django.db.models.fields.DecimalField')(max_digits=10, decimal_places=3, null=True)),
            ('weight', self.gf('django.db.models.fields.DecimalField')(max_digits=10, decimal_places=3, null=True)),
            ('length', self.gf('django.db.models.fields.DecimalField')(max_digits=10, decimal_places=3, null=True)),
            ('views', self.gf('django.db.models.fields.BigIntegerField')(null=True)),
            ('listing_id', self.gf('django.db.models.fields.BigIntegerField')(unique=True)),
            ('favorers', self.gf('django.db.models.fields.BigIntegerField')(null=True)),
            ('quantity', self.gf('django.db.models.fields.IntegerField')(null=True)),
            ('price', self.gf('django.db.models.fields.DecimalField')(max_digits=10, decimal_places=3)),
            ('recipient', self.gf('django.db.models.fields.CharField')(max_length=20, null=True)),
            ('occasion', self.gf('django.db.models.fields.CharField')(max_length=20, null=True)),
            ('made_by', self.gf('django.db.models.fields.CharField')(max_length=20, null=True)),
            ('has_variations', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('is_customizable', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('category', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['etsy.Category'], null=True)),
            ('document_index', self.gf('django.db.models.fields.IntegerField')(default=-1)),
        ))
        db.send_create_signal('etsy', ['Listing'])

        # Adding M2M table for field styles on 'Listing'
        m2m_table_name = db.shorten_name('etsy_listing_styles')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('listing', models.ForeignKey(orm['etsy.listing'], null=False)),
            ('style', models.ForeignKey(orm['etsy.style'], null=False))
        ))
        db.create_unique(m2m_table_name, ['listing_id', 'style_id'])

        # Adding M2M table for field tags on 'Listing'
        m2m_table_name = db.shorten_name('etsy_listing_tags')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('listing', models.ForeignKey(orm['etsy.listing'], null=False)),
            ('tag', models.ForeignKey(orm['etsy.tag'], null=False))
        ))
        db.create_unique(m2m_table_name, ['listing_id', 'tag_id'])

        # Adding M2M table for field materials on 'Listing'
        m2m_table_name = db.shorten_name('etsy_listing_materials')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('listing', models.ForeignKey(orm['etsy.listing'], null=False)),
            ('material', models.ForeignKey(orm['etsy.material'], null=False))
        ))
        db.create_unique(m2m_table_name, ['listing_id', 'material_id'])


    def backwards(self, orm):
        # Deleting model 'Tag'
        db.delete_table('etsy_tag')

        # Deleting model 'Category'
        db.delete_table('etsy_category')

        # Deleting model 'Style'
        db.delete_table('etsy_style')

        # Deleting model 'Material'
        db.delete_table('etsy_material')

        # Deleting model 'Listing'
        db.delete_table('etsy_listing')

        # Removing M2M table for field styles on 'Listing'
        db.delete_table(db.shorten_name('etsy_listing_styles'))

        # Removing M2M table for field tags on 'Listing'
        db.delete_table(db.shorten_name('etsy_listing_tags'))

        # Removing M2M table for field materials on 'Listing'
        db.delete_table(db.shorten_name('etsy_listing_materials'))


    models = {
        'etsy.category': {
            'Meta': {'object_name': 'Category'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        },
        'etsy.listing': {
            'Meta': {'ordering': "['-views']", 'object_name': 'Listing'},
            'category': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['etsy.Category']", 'null': 'True'}),
            'description': ('django.db.models.fields.CharField', [], {'max_length': '2000'}),
            'document_index': ('django.db.models.fields.IntegerField', [], {'default': '-1'}),
            'favorers': ('django.db.models.fields.BigIntegerField', [], {'null': 'True'}),
            'has_variations': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'height': ('django.db.models.fields.DecimalField', [], {'max_digits': '10', 'decimal_places': '3', 'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_customizable': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'length': ('django.db.models.fields.DecimalField', [], {'max_digits': '10', 'decimal_places': '3', 'null': 'True'}),
            'listing_id': ('django.db.models.fields.BigIntegerField', [], {'unique': 'True'}),
            'made_by': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True'}),
            'materials': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['etsy.Material']", 'symmetrical': 'False'}),
            'occasion': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True'}),
            'price': ('django.db.models.fields.DecimalField', [], {'max_digits': '10', 'decimal_places': '3'}),
            'quantity': ('django.db.models.fields.IntegerField', [], {'null': 'True'}),
            'recipient': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True'}),
            'styles': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['etsy.Style']", 'symmetrical': 'False'}),
            'tags': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['etsy.Tag']", 'symmetrical': 'False'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'views': ('django.db.models.fields.BigIntegerField', [], {'null': 'True'}),
            'weight': ('django.db.models.fields.DecimalField', [], {'max_digits': '10', 'decimal_places': '3', 'null': 'True'}),
            'width': ('django.db.models.fields.DecimalField', [], {'max_digits': '10', 'decimal_places': '3', 'null': 'True'})
        },
        'etsy.material': {
            'Meta': {'object_name': 'Material'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        },
        'etsy.style': {
            'Meta': {'object_name': 'Style'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        },
        'etsy.tag': {
            'Meta': {'object_name': 'Tag'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        }
    }

    complete_apps = ['etsy']
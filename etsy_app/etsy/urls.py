from django.conf.urls import patterns, url

from etsy_app.etsy import views

urlpatterns = patterns('',
    url(r'^$', views.index, name='index'),
    url(r'listings/(?P<category_id>\d+)/$', views.listings, name='listings'),
    url(r'listing/(?P<list_id>\d+)$', views.listing, name='listing'),
    url(r'listing/(?P<list_id>\d+)/keywords/$', views.listing_keywords, name='listing_keywords'),
    url(r'listing/(?P<list_id>\d+)/similar/$', views.listing_similar, name='listing_similar'),
    url(r'listing/(?P<list_id>\d+)/tags/$', views.recommend_tags, name='recommend_tags'),
    url(r'listing/(?P<list_id>\d+)/tags_title/$', views.recommend_tags_title, name='recommend_tags_title'),
    url(r'listing/(?P<list_id>\d+)/tags_category/$', views.recommend_tags_category, name='recommend_tags_categroy'),
    url(r'listing/(?P<list_id>\d+)/topics/$', views.listing_topics, name='listing_topics'),
    url(r'listings/test/$', views.test_listings, name='test_listings'),
)
from django.http import HttpResponse
from django.template import RequestContext, loader
from django.db.models import Count

from etsy_app.etsy.models import *
from etsy_app.extract import *



def index(request):
    categories = Category.objects.annotate(listings_count=Count('listing')).filter(listings_count__gte=100).order_by('-listings_count')
    template = loader.get_template('etsy/index.html')
    context = RequestContext(request, {
        'categories': categories,
    })

    return HttpResponse(template.render(context))


def listings(request, category_id):
    category = Category.objects.get(id=category_id)
    listings = Listing.objects.filter(category__name=category.name)
    template = loader.get_template('etsy/listings.html')
    context = RequestContext(request, {
        'category': category,
        'listings': listings,
    })
    
    return HttpResponse(template.render(context))


def test_listings(request):
    categories = Category.objects.annotate(listings_count=Count('listing')).filter(listings_count__gte=100).order_by('-listings_count')
    listings = []

    for category in categories:
        listings.extend(Listing.objects.filter(category__name=category.name, document_index=-1))
        
    template = loader.get_template('etsy/listings.html')
    context = RequestContext(request, {
        'listings': listings,
        'test_set': True
    })
    
    return HttpResponse(template.render(context))


def listing(request, list_id):
    listing = Listing.objects.get(id=list_id)
    template = loader.get_template('etsy/listing.html')
    context = RequestContext(request, {
        'listing': listing,
    })
    
    return HttpResponse(template.render(context))


def listing_keywords(request, list_id):
    listing = Listing.objects.get(id=list_id)
    keywords = find_keywords(listing)
    template = loader.get_template('etsy/listing.html')
    context = RequestContext(request, {
        'listing': listing,
        'keywords': keywords
    })
    
    return HttpResponse(template.render(context))


def recommend_tags(request, list_id):
    listing = Listing.objects.get(id=list_id)
    tags = find_tags_from_similar(listing)
    template = loader.get_template('etsy/listing.html')
    context = RequestContext(request, {
        'listing': listing,
        'recommended_tags': tags
    })

    return HttpResponse(template.render(context))


def recommend_tags_title(request, list_id):
    listing = Listing.objects.get(id=list_id)
    tags = find_tags(listing, listing.title)
    template = loader.get_template('etsy/listing.html')
    context = RequestContext(request, {
        'listing': listing,
        'recommended_tags': tags
    })

    return HttpResponse(template.render(context))


def recommend_tags_category(request, list_id):
    listing = Listing.objects.get(id=list_id)
    tags = find_tags(listing, listing.category.name, 10)
    template = loader.get_template('etsy/listing.html')
    context = RequestContext(request, {
        'listing': listing,
        'recommended_tags': tags
    })

    return HttpResponse(template.render(context))


def listing_similar(request, list_id):
    listing = Listing.objects.get(id=list_id)
    if listing.document_index == -1:
        similar = find_similar(listing, 5)
    else:
        similar = find_similar_by_index(listing, 6)

    similar_items = [Listing.objects.get(document_index=index) for (index, metric) in similar 
                        if index != listing.document_index]
    template = loader.get_template('etsy/listing.html')
    context = RequestContext(request, {
        'listing': listing,
        'similar': similar_items
    })
    
    return HttpResponse(template.render(context))


def listing_topics(request, list_id):
    listing = Listing.objects.get(id=list_id)
    topics = find_latent_topics(listing)

    template = loader.get_template('etsy/listing.html')
    context = RequestContext(request, {
        'listing': listing,
        'topics': topics
    })
    
    return HttpResponse(template.render(context))


def classify_keywords(request, list_id):
    #TODO better classifier
    listing = Listing.objects.get(id=list_id)
    keywords = ' '.join([word for word, metric in find_keywords(listing, 20)])
    category = suggest_category(keywords)
    template = loader.get_template('etsy/listing.html')
    context = RequestContext(request, {
        'listing': listing,
        'suggested_category': None
    })
    
    return HttpResponse(template.render(context))





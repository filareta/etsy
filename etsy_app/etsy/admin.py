from django.contrib import admin
from etsy_app.etsy.models import *


admin.site.register(Tag)
admin.site.register(Category)
admin.site.register(Material)
admin.site.register(Style)
admin.site.register(Listing)
from django.db import models

# Create your models here.

class Tag(models.Model):
    name = models.CharField(max_length=200)

    def __str__(self):
        return self.name


class Category(models.Model):
    name = models.CharField(max_length=200)

    def __str__(self):
        return self.name


class Style(models.Model):
    name = models.CharField(max_length=200)

    def __str__(self):
        return self.name


class Material(models.Model):
    name = models.CharField(max_length=200)

    def __str__(self):
        return self.name


class Listing(models.Model):
    RECIPIENT = (
            ('MEN', "men"), ('WOMEN', "women"), ('UNISEX_ADULT', "unisex_adult"), 
            ('TEEN_BOYS', "teen_boys"), ('TEEN_GIRLS', "teen_girls"), 
            ('TEENS', "teens"), ('GIRLS', "girls"), ('CHILDREN', "children"), 
            ('BABY_BOYS', "baby_boys"), ('BABY_GIRLS', "baby_girls"), ('BABIES', "babies"), 
            ('BIRDS', "birds"), ('CATS', "cats"), ('DOGS', "dogs"), 
            ('PETS', "pets"), ('NOT_SPECIFIED', "not_specified")
        )

    OCCASION = (
            ('ANNIVERSARY', "anniversary"), ('BAPTISM', "baptism"), ('BAR_OR_BAT_MITZVAH', "bar_or_bat_mitzvah"), 
            ('BIRTHDAY', "birthday"), ('CANDA_DAY', "canada_day"), ('CHINESE_NEW_YEAR', "chinese_new_year"), 
            ('CINCO_DE_MAYO', "cinco_de_mayo"), ('CONFIRMATION', "confirmation"), ('CHRISTMAS', "christmas"), 
            ('DAY_OF_THE_DEAD', "day_of_the_dead"), ('EASTER', "easter"), ('EID', "eid"), ('ENGAGEMENT', "engagement"), 
            ('FATHERS_DAY', "fathers_day"), ('GET_WELL', "get_well"), ('GRADUATION', "graduation"), 
            ('HALOWEEN', "halloween"), ('HANUKKAH', "hanukkah"), ('HOUSEWARMING', "housewarming"), ('KWANZAA', "kwanzaa"), 
            ('PROM', "prom"), ('JULY_4TH', "july_4th"), ('MOTHERS_DAY', "mothers_day"), ('NEW_BABY',"new_baby"), 
            ('NEW_YEARS', "new_years"), ('QUINCEANERA', "quinceanera"), ('RETIREMENT', "retirement"), 
            ('ST_PATRICKS_DAY', "st_patricks_day"), ('SWEET_16', "sweet_16"), ('SYMPATHY', "sympathy"), 
            ('THANKSGIVING', "thanksgiving"), ('VALENTINES', "valentines"), ('WEDDING', "wedding")
        )

    WHO_MADE = (
            ('I_DID', "i_did"), 
            ('COLLECTIVE', "collective"), 
            ('SOMEONE_ELSE', "someone_else")
        )

    class Meta:
        ordering = ['-views']

    title = models.CharField(max_length=200)
    description = models.CharField(max_length=2000)
    width = models.DecimalField(max_digits=10, decimal_places=3, null=True)
    height = models.DecimalField(max_digits=10, decimal_places=3, null=True)
    weight = models.DecimalField(max_digits=10, decimal_places=3, null=True)
    length = models.DecimalField(max_digits=10, decimal_places=3, null=True)
    views = models.BigIntegerField(null=True)
    listing_id = models.BigIntegerField(unique=True)
    favorers = models.BigIntegerField(null=True)
    quantity = models.IntegerField(null=True)
    price = models.DecimalField(max_digits=10, decimal_places=3, null=False)
    styles = models.ManyToManyField(Style, verbose_name="list of styles")
    tags = models.ManyToManyField(Tag, verbose_name="list of tags")
    materials = models.ManyToManyField(Material, verbose_name="list of materials")
    recipient = models.CharField(max_length=20, choices=RECIPIENT, null=True)
    occasion = models.CharField(max_length=20, choices=OCCASION, null=True)
    made_by = models.CharField(max_length=20, choices=WHO_MADE, null=True)
    has_variations = models.BooleanField(default=False)
    is_customizable = models.BooleanField(default=True)
    category = models.ForeignKey(Category, null=True)
    document_index = models.IntegerField(default=-1)
    corpus_name = models.CharField(max_length=100, default="")

    def __str__(self):
        return self.title

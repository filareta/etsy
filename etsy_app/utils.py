import logging
from gensim import corpora, similarities, models, utils
from pprint import pprint
from collections import defaultdict
import nltk
from nltk.stem.wordnet import WordNetLemmatizer
from nltk.corpus import wordnet
import re


logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)
RELEVANT_CATEGORIES = set([wordnet.ADJ, wordnet.NOUN])


def get_wordnet_pos(treebank_tag):
    if treebank_tag.startswith('J'):
        return wordnet.ADJ
    elif treebank_tag.startswith('V'):
        return wordnet.VERB
    elif treebank_tag.startswith('N'):
        return wordnet.NOUN
    elif treebank_tag.startswith('R'):
        return wordnet.ADV
    else:
        return ''


def process_document(document, stop_words):
    lmtz = WordNetLemmatizer()
    document = re.sub(r'([^:]+:)?//([^.]+\.)+[^/]+/([^/\s]+/)*([^/\s]+)', ' ', document)
    document = re.sub(r'[*!@#$%=_/~\-+:|]+', ' ', document)
    document = re.sub(u"[\u0400-\u0500]+", ' ', document)
    
    tokens = [token for token in list(utils.tokenize(utils.to_utf8(document), deacc=True, to_lower=True))
              if token not in stop_words]
    tagged = nltk.pos_tag(tokens)
    pprint(tagged)

    lemmatized = [lmtz.lemmatize(token, get_wordnet_pos(tag)) for token, tag in tagged 
                    if get_wordnet_pos(tag) in RELEVANT_CATEGORIES]


    return lemmatized


def load_stop_words(filename):
    with open(filename) as f:
        return [word.rstrip('\n') for word in f.readlines()]


def filter_by_frequency(tokens):
    frequencies = defaultdict(int)

    for token in tokens:
            frequencies[token] += 1

    return [token for token in tokens if frequencies[token] > 1]

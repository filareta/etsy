from django.db.models import Q
from django.db.models import Count
from etsy_app.etsy.models import *

from textblob.classifiers import NaiveBayesClassifier
from etsy_app.extract import find_keywords
from etsy_app.document import Document
from etsy_app.key_phrases_extraction import extract_candidate_words
from random import shuffle
import pickle


TAGS=set(['JJ','JJR','JJS','NN','NNP','NNS','NNPS', 'VBD', 'VBN', 'VBP', 'VBG'])

def suggest_category(text):
    pass


def train_with_keyords():
    categories = Category.objects.annotate(listings_count=Count('listing')).filter(
        listings_count__gte=100).order_by('-listings_count')

    train = []

    for category in categories:
        listings = Listing.objects.filter(category__name=category.name).filter(~Q(document_index=-1))[:50]
        
        documents = [Document(listing.title, listing.description, 
                          map(lambda x: x.name, listing.styles.all()),
                          map(lambda x: x.name, listing.materials.all()), 
                          listing.listing_id, 
                          listing.occasion, listing.recipient) for listing in listings]
        train.extend([(' '.join(extract_candidate_words(doc.build_repr(), TAGS)), category.name) 
                      for doc in documents])


    print("Start training...")
    shuffle(train)
    cl = NaiveBayesClassifier(train)

    return cl


def build_test_set():
    categories = Category.objects.annotate(listings_count=Count('listing')).filter(
        listings_count__gte=100).order_by('-listings_count')

    test = []

    for category in categories:
        listings = Listing.objects.filter(category__name=category.name).filter(document_index=-1)[:5]
        
        documents = [Document(listing.title, listing.description, 
                          map(lambda x: x.name, listing.styles.all()),
                          map(lambda x: x.name, listing.materials.all()), 
                          listing.listing_id, 
                          listing.occasion, listing.recipient) for listing in listings]
        test.extend([(' '.join(extract_candidate_words(doc.build_repr(), TAGS)), category.name) 
            for doc in documents])

    return test

def store(cl):
    print("Store classifier...")
    cls_file = open('classifier.obj', 'wb')
    pickle.dump(cl, cls_file)

def accuracy():
    cl = train_with_keyords()
    print("Building test set...")
    test = build_test_set()
    print("Start evaluation...")
    print("Accuracy: {0}".format(cl.accuracy(test)))
    store(cl)
   






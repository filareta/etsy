from django.conf import settings
import os, json

# os.environ.setdefault("DJANGO_SETTINGS_MODULE", "settings")

from django.db import models
from etsy_app.etsy.models import *
from etsy_app.settings import DATA_STORAGE_DIR


def load_json_file(json_file):
    with open(json_file, 'r', encoding="cp866") as jfile:
        data = json.load(jfile)

    return data

def json_items_to_docs(items):
    documents = []
    for item in items:
        documents.append(Document(item["title"], item["description"], item["style"], item["materials"], item["tags"], item["listing_id"]))

    return documents

def load_data(storage_dir):
    for data_file in os.listdir(storage_dir):
        if os.path.isfile(os.path.join(data_dir, data_file)) and data_file.lower().endswith('.json'):
            json_data = load_json_file(data_file)


def create_listing(json_dict):
    if json_dict['state'] != 'active':
        return

    tags = []
    materials = []
    styles = []
    category = None

    if 'tags' in json_dict and json_dict['tags']:
        for tag in json_dict['tags']:
            t, created = Tag.objects.get_or_create(name=tag)
            if not created:
                t.save()
            tags.append(t)

    if 'materials' in json_dict and json_dict['materials']:
        for material in json_dict['materials']:
            m, created = Material.objects.get_or_create(name=material)

            if not created:
                m.save()
            materials.append(m)

    if 'style' in json_dict and json_dict['style']:
        for style in json_dict['style']:
            s, created = Style.objects.get_or_create(name=style)
            if not created:
                s.save()
            styles.append(s)

    if 'category_id' in json_dict and json_dict['category_id'] and \
        'category_path_ids' in json_dict and json_dict['category_path_ids'] and \
        'category_path' in json_dict and json_dict['category_path']:
        for i, path_id in enumerate(json_dict['category_path_ids']):
            if path_id == json_dict['category_id']:
                category, created = Category.objects.get_or_create(name=json_dict['category_path'][i])
                if not created:
                    category.save()

    if not 'title' in json_dict and not 'description' in json_dict:
        return

    if Listing.objects.filter(listing_id=json_dict['listing_id']).exists():
        return

    # print(json_dict)
    price = json_dict['price'] and float(json_dict['price'])
    width = json_dict['item_width'] and float(json_dict['item_width'])
    weight = json_dict['item_weight'] and float(json_dict['item_weight'])
    height = json_dict['item_height'] and float(json_dict['item_height'])
    length = json_dict['item_length'] and float(json_dict['item_length'])
        
    listing = Listing.objects.create(
        title=json_dict['title'], listing_id=json_dict['listing_id'],
        description=json_dict['description'], price=price,
        width=width, height=height,
        weight=weight, length=length,
        quantity=json_dict['quantity'], favorers=json_dict['num_favorers'],
        category=category, made_by=json_dict['who_made'], 
        occasion=json_dict['occasion'],
        recipient=json_dict['recipient'], views=json_dict['views'],
        has_variations=json_dict['has_variations'],
        is_customizable=json_dict['is_customizable']
    )

    listing.save()

    for tag in tags:
        listing.tags.add(tag)

    for style in styles:
        listing.styles.add(style)

    for material in materials:
        listing.materials.add(material)


def load_from_dir(storage_path):
    for data_file in os.listdir(storage_path):
        try:
            data = load_json_file(os.path.join(storage_path, data_file))

            for result in data["results"]:
                create_listing(result)
        except UnicodeDecodeError as e:
            print("Will skip file with name {} due to an exception {}".format(data_file, e))


if __name__ == '__main__':
    load_from_dir(DATA_STORAGE_DIR)
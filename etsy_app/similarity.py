from etsy_app.key_phrases_extraction import *
from etsy_app.utils import filter_by_frequency
import gensim, logging


logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)


def score_document_similarity(text, corpus_path, stop_words, topn):
    boc_text = extract_candidate_chunks(text, stop_words=stop_words)

    dictionary = gensim.corpora.Dictionary.load(corpus_path + '.dict')
    text_bow = dictionary.doc2bow(boc_text)
    index = gensim.similarities.docsim.Similarity.load(corpus_path)
    index.num_best = topn

    return index[text_bow]


def score_document_similarity_in_index(doc_id, corpus_path, topn):
    index = gensim.similarities.docsim.Similarity.load(corpus_path)
    index.num_best = topn

    return index.similarity_by_id(doc_id)


def score_document_similarity(doc, corpus_path, stop_words, topn):
    index = gensim.similarities.docsim.Similarity.load(corpus_path)
    index.num_best = topn

    text = extract_candidate_chunks(doc, stop_words=stop_words)
    dictionary = gensim.corpora.Dictionary.load(corpus_path + '.dict')
    query = dictionary.doc2bow(text)

    return index[query]


def extract_latent_topics(doc, corpus_path, stop_words, probability=0.3, topn=5):
    boc_text = extract_candidate_chunks(doc, stop_words=stop_words)

    dictionary = gensim.corpora.Dictionary.load(corpus_path + '.dict')
    query = dictionary.doc2bow(boc_text)

    lda = gensim.models.ldamodel.LdaModel.load(corpus_path)
    topics = lda.get_document_topics(query, probability)
    topic_terms = [term_tuple for topic, score in topics 
                   for term_tuple in lda.get_topic_terms(topic, topn)]
    terms = [dictionary.get(t_id) for t_id, score in topic_terms]

    return terms


def store_similarity_model(texts, corpus_path, stop_words):
    boc_texts = [extract_candidate_chunks(text, stop_words=stop_words) for text in texts]

    dictionary = gensim.corpora.Dictionary(boc_texts)
    once_ids = [tokenid for tokenid, docfreq in dictionary.dfs.items() if docfreq == 1]
    dictionary.filter_tokens(once_ids) # remove stop words and words that appear only once
    dictionary.compactify() # remove gaps in id sequence after words that were removed
    dictionary.save(corpus_path + '.dict')
    corpus = [dictionary.doc2bow(boc_text) for boc_text in boc_texts]

    similarity = gensim.similarities.docsim.Similarity(corpus_path, corpus, num_features=len(dictionary.keys()))

    similarity.save()


def store_lda_model(texts, corpus_path, stop_words):
    boc_texts = [extract_candidate_chunks(text, stop_words=stop_words) for text in texts]

    dictionary = gensim.corpora.Dictionary(boc_texts)
    once_ids = [tokenid for tokenid, docfreq in dictionary.dfs.items() if docfreq == 1]
    dictionary.filter_tokens(once_ids) # remove stop words and words that appear only once
    dictionary.compactify() # remove gaps in id sequence after words that were removed
    dictionary.save(corpus_path + '.dict')
    corpus = [dictionary.doc2bow(boc_text) for boc_text in boc_texts]
    
    lda = gensim.models.ldamodel.LdaModel(corpus, num_topics=20, id2word=dictionary,
                                           eval_every=1, iterations=100, passes=2)
    lda.save(corpus_path)





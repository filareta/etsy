class Document:
    #includes title, description, materials and styles
    def __init__(self, title, description, styles, materials, 
        listing_id, occasion, recipient):
        self.title = title or ""
        self.description = description or ""
        self.styles = styles or []
        self.materials = materials or []
        self.listing_id = listing_id
        self.occasion = occasion or ""
        self.recipient = recipient or ""

    def build_repr(self):
        return "{}. {} {} {} {} {}".format(self.title, self.description, 
            " ".join(self.styles), " ".join(self.materials),
            self.occasion, self.recipient)
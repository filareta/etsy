from django.conf import settings
import os, random
from django.db.models import Q


from etsy_app.etsy.models import *
from etsy_app.key_phrases_extraction import *
from etsy_app.utils import load_stop_words
from etsy_app.similarity import *
from gensim.models import Word2Vec
import gensim, logging


logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)


STOP_WORDS = load_stop_words('etsy_app/stop_list_en')
KEY_WORDS_SUMMARIES = './summary'
CORPUSES = 'etsy_app/corpuses'
SIMILARITY_INDEX = 'similarity_index'
WORD2VEC_CORPUS = 'word2vec_model'
LDA_CORPUS = 'lda_model'


def build_corpuses(categories):
    for category in categories:
        extract_keywords_for_category(category.name)


def extract_keywords_for_category(category):
    listings = Listing.objects.filter(category__name=category)

    print("Will extract words and phrases for category {}".format(category))
    documents = [Document(listing.title, listing.description, 
                          map(lambda x: x.name, listing.styles.all()),
                          map(lambda x: x.name, listing.materials.all()), 
                          listing.listing_id, 
                          listing.occasion, listing.recipient) for listing in listings]

    doc_texts = [doc.build_repr() for doc in documents]
    store_tfidf_model(doc_texts, os.path.join(CORPUSES, category), stop_words=STOP_WORDS)


def find_keywords(listing, topn=10):
    document = Document(listing.title, listing.description, 
                          map(lambda x: x.name, listing.styles.all()),
                          map(lambda x: x.name, listing.materials.all()), 
                          listing.listing_id, 
                          listing.occasion, listing.recipient)

    return score_words_in_text(document.build_repr(), os.path.join(CORPUSES, listing.category.name), STOP_WORDS, topn)


def find_similar_by_index(listing, topn=10):
    return score_document_similarity_in_index(listing.document_index, os.path.join(CORPUSES, SIMILARITY_INDEX), topn)


def find_similar(listing, topn=10):
    document = Document(listing.title, listing.description, 
                          map(lambda x: x.name, listing.styles.all()),
                          map(lambda x: x.name, listing.materials.all()), 
                          listing.listing_id, 
                          listing.occasion, listing.recipient)

    return score_document_similarity(document.build_repr(), os.path.join(CORPUSES, SIMILARITY_INDEX), STOP_WORDS, topn)


def build_similarity_corpus(categories):
    documents = []
    doc_index = 0
    for category in categories:
        listings = Listing.objects.filter(category__name=category.name)
        test_count = len(listings) / 6
        leave_for_test = lambda x: x < test_count and random.uniform(0, 1) > 0.6
        for_test = 0

        for listing in listings:
            if leave_for_test(for_test):
                listing.document_index = -1
                for_test += 1
            else:
                document = Document(listing.title, listing.description, 
                              map(lambda x: x.name, listing.styles.all()),
                              map(lambda x: x.name, listing.materials.all()), 
                              listing.listing_id, 
                              listing.occasion, listing.recipient)

                listing.document_index = doc_index
                doc_index += 1
                documents.append(document.build_repr())
            
            listing.save()


    store_similarity_model(documents, os.path.join(CORPUSES, SIMILARITY_INDEX), STOP_WORDS)


def process_trainig_docs(categories):
    documents = []
    doc_index = 0
    for category in categories:
        listings = Listing.objects.filter(category__name=category.name).filter(~Q(document_index=-1))
        

        documents_per_category = [Document(listing.title, listing.description, 
                                  map(lambda x: x.name, listing.styles.all()),
                                  map(lambda x: x.name, listing.materials.all()), 
                                  listing.listing_id, 
                                  listing.occasion, listing.recipient).build_repr()
                                  for listing in listings]
        
        documents.extend(documents_per_category)

    return documents


def build_word2vec_model(categories):
    docs = []

    for category in categories:
        listings = Listing.objects.filter(category__name=category.name).filter(~Q(document_index=-1))
        
        keywords = [[word for word, metric in find_keywords(listing, 15)] for listing in listings]

        docs.extend(keywords)

    word2vec = gensim.models.Word2Vec(docs, size=200, window=5, min_count=5, 
                                       alpha=0.05, sg=1, iter=50, workers=4)

    word2vec.save(os.path.join(CORPUSES, WORD2VEC_CORPUS))


def build_lda_model(categories):
    store_lda_model(process_trainig_docs(categories),
        os.path.join(CORPUSES, LDA_CORPUS), STOP_WORDS)


def find_tags(listing, text, top=3):
    recommended_tags = []

    word2vec = Word2Vec.load(os.path.join(CORPUSES, WORD2VEC_CORPUS))

    words = extract_candidate_words(text, stop_words=STOP_WORDS)

    tags = []
    for kword in words:
        try:
            suggested = word2vec.most_similar(kword, topn=top)
            for word, sim in suggested:
                tags.append(word)
        except KeyError as e:
            print(e)
    
    return set(tags)


def find_tags_from_similar(listing, top_similar=3):
    similar = find_similar(listing, top_similar)
    listings = [Listing.objects.get(document_index=doc_ind) for (doc_ind, score) in similar]

    all_tags = [tag.name.lower() for listing in listings for tag in listing.tags.all()]

    return set(all_tags)


def find_latent_topics(listing):
    document = Document(listing.title, listing.description, 
                          map(lambda x: x.name, listing.styles.all()),
                          map(lambda x: x.name, listing.materials.all()), 
                          listing.listing_id, 
                          listing.occasion, listing.recipient)

    return extract_latent_topics(document.build_repr(), os.path.join(CORPUSES, LDA_CORPUS), STOP_WORDS)
